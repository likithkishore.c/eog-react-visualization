import React, { FC, memo } from 'react';
import {
  ApolloClient, ApolloProvider, gql, HttpLink, InMemoryCache, split, useSubscription,
} from '@apollo/client';
import {
  Grid, Typography, Card, CardHeader, CardContent,
} from '@material-ui/core';
import LinearProgress from '@material-ui/core/LinearProgress';
import { WebSocketLink } from '@apollo/client/link/ws';
import { getMainDefinition } from '@apollo/client/utilities';

const wsLink = new WebSocketLink({
  uri: 'ws://react.eogresources.com/graphql',
  options: {
    reconnect: true,
  },
});
const httpLink = new HttpLink({
  uri: 'https://react.eogresources.com/graphql',
});

const splitLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === 'OperationDefinition'
      && definition.operation === 'subscription'
    );
  },
  wsLink,
  httpLink,
);

const client = new ApolloClient({
  link: splitLink,
  cache: new InMemoryCache(),
});

type IProps = {
  metrics: String[];
};
interface MIProps{
  metric: String,
}
interface TIProps extends MIProps{
  data: any,
}

const query = gql`
 subscription{
    newMeasurement{
        at
        metric
        value
        unit
    }
 }
`;
const areEqual = (pre:any, next:any) => {
  console.log(next.metric, next.data?.metric);
  return next.metric === next.data?.metric;
};

const Temperatue: FC<TIProps> = memo(({ data }) => <CardContent>{ data?.value }</CardContent>,
  areEqual);

const Metric:FC<IProps> = ({ metrics }) => {
  const { loading, error, data } = useSubscription<any>(query);
  if (loading) return <LinearProgress />;
  if (error) return <Typography color="error">error</Typography>;
  return (
    <Grid container justifyContent="center" alignItems="center" spacing={2}>
      {metrics.map((metric, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <Grid item key={index} sm={4}>
          <Card>
            <CardHeader title={metric} />
            <CardContent>
              {/* { data?.newMeasurement?.metric === metric && data?.newMeasurement?.value } */}
              <Temperatue metric={metric} data={data?.newMeasurement} />
            </CardContent>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};
export default ({ metrics }: IProps) => (
  <ApolloProvider client={client}>
    <Metric metrics={metrics} />
  </ApolloProvider>
);
