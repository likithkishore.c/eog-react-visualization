import React, { FC } from 'react';
import {
  ApolloClient, ApolloProvider, useQuery, gql, InMemoryCache,
} from '@apollo/client';
import { Typography } from '@material-ui/core';
import LinearProgress from '@material-ui/core/LinearProgress';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const client = new ApolloClient({
  uri: 'https://react.eogresources.com/graphql',
  cache: new InMemoryCache(),
});

type Metrics = {
  getMetrics: String[];
};

const query = gql`
  query {
    getMetrics
  }
`;

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    width: 600,
    '& > * + *': {
      marginTop: theme.spacing(3),
    },
  },
  select: {
    background: '#fff',
  },
}));

type IProp = {
  handleChange: (value: String[])=>void;
};

const Search: FC<IProp> = ({ handleChange }) => {
  const classes = useStyles();
  const { loading, error, data } = useQuery<Metrics>(query);
  if (loading) return <LinearProgress />;
  if (error) return <Typography color="error">error</Typography>;
  return (
    <div className={classes.root}>
      <Autocomplete
        multiple
        classes={{ root: classes.select }}
        options={data?.getMetrics as any[]}
        getOptionLabel={(option) => option}
        filterSelectedOptions
        onChange={(_, value) => {
          if (value) handleChange(value);
        }}
        renderInput={(params) => (
          // eslint-disable-next-line react/jsx-props-no-spreading
          <TextField {...params} variant="outlined" placeholder="Select.." />
        )}
      />
    </div>
  );
};
export default ({ handleChange }: IProp) => (
  <ApolloProvider client={client}>
    <Search handleChange={handleChange} />
  </ApolloProvider>
);
