import React, { useState } from 'react';
import { ToastContainer } from 'react-toastify';
import { MuiThemeProvider, createTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import {
  Container, Grid, makeStyles,
} from '@material-ui/core';

import 'react-toastify/dist/ReactToastify.css';
import Header from './components/Header';
import Wrapper from './components/Wrapper';
import Search from './Features/Search/Search';
import Metric from './Features/Metric/Metric';

const theme = createTheme({
  palette: {
    primary: {
      main: 'rgb(39,49,66)',
    },
    secondary: {
      main: 'rgb(197,208,222)',
    },
    background: {
      default: 'rgb(226,231,238)',
    },
  },
});

const useStyles = makeStyles({
  container: {
    marginTop: 30,
  },
});

const App = () => {
  const classes = useStyles();
  const [metrics, setMatrics] = useState<String[]>([]);
  const handleChange = (newValue: String[]) => {
    setMatrics(newValue);
  };

  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      <Wrapper>
        <Header />
        <Container>
          <Grid container className={classes.container} spacing={2}>
            <Grid item sm={6}>
              <Metric metrics={metrics} />
            </Grid>
            <Grid item sm={6}>
              <Search handleChange={handleChange} />
            </Grid>
          </Grid>
        </Container>
        <ToastContainer />
      </Wrapper>
    </MuiThemeProvider>
  );
};
export default App;
